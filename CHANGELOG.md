# Change Log

All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](http://keepachangelog.com/) and this project adheres to [Semantic Versioning](http://semver.org/).

## [v1.0.0-alpha4] - 2018-04-04

* Changing `auto_wait_for` to `auto_refresh`, which now is only applied to requests that store something in Elasticsearch
  * There was an issue with encouraging the use of `refresh=auto_wait`, so instead we'll trigger a refresh with `refresh=true`
  * Read more at https://gitlab.com/gt8/open-source/elixir/bungee/issues/20

## [v1.0.0-alpha3] - 2018-04-04

* Allowing `:auto_wait_for` to be configured globally
  * Use this in your `:test` environment and it will ensure `save` and `delete` requests wait for Elasticsearch to refresh index before returning. You'll want to create your indexes with a refresh interval of less than 10ms, probably.
