defmodule Bungee.Response.Validator do
  require Logger

  def response_ok?(response = %Tesla.Env{status: 404}) do
    Logger.info(fn -> "Bungee response HTTP 404 - :not_found" end)
    Logger.debug(fn -> "\tResponse: #{inspect(response)}" end)

    {response, :not_found}
  end

  def response_ok?(response = %Tesla.Env{status: 200, body: %{"result" => "updated"}}) do
    Logger.info(fn -> "Bungee response HTTP 200 - :updated" end)
    Logger.debug(fn -> "\tResponse: #{inspect(response)}" end)

    {response, :updated}
  end

  def response_ok?(response = %Tesla.Env{status: 200, body: %{"result" => "deleted"}}) do
    Logger.info(fn -> "Bungee response HTTP 200 - :deleted" end)
    Logger.debug(fn -> "\tResponse: #{inspect(response)}" end)

    {response, :deleted}
  end

  def response_ok?(response = %Tesla.Env{status: 200}) do
    Logger.info(fn -> "Bungee response HTTP 200" end)
    Logger.debug(fn -> "\tResponse: #{inspect(response)}" end)

    {response, :ok}
  end

  def response_ok?(response = %Tesla.Env{status: 201}) do
    Logger.info(fn -> "Bungee response HTTP 201" end)
    Logger.debug(fn -> "\tResponse: #{inspect(response)}" end)

    {response, :created}
  end

  def response_ok?(response = %Tesla.Env{status: 202}) do
    Logger.info(fn -> "Bungee response HTTP 202" end)
    Logger.debug(fn -> "\tResponse: #{inspect(response)}" end)

    {response, :deleted}
  end

  def response_ok?(response = %Tesla.Env{status: status}) do
    Logger.info(fn -> "Bungee response HTTP #{status}" end)
    Logger.debug(fn -> "\tResponse: #{inspect(response)}" end)

    {response, :error}
  end
end
