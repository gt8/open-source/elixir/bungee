defmodule Bungee.Response.Decoder do
  def decode([], module) do
    {:error, :not_found}
  end

  def decode(
        documents,
        count,
        module
      ) do
    {:ok, count, decode_documents(documents, [], module)}
  end

  def decode(body, module) do
    {:ok, decode_document(body, module)}
  end

  def decode!(body, module) do
    decode_document(body, module)
  end

  def decode!(
        documents,
        count,
        module
      ) do
    decode_documents(documents, [], module)
  end

  def decode(unknown) do
    raise ArgumentError, message: "No idea what I'm decoding (Value: #{inspect(unknown)})"
  end

  defp decode_documents([head | tail], carry, module) do
    decode_documents(tail, carry ++ [decode_document(head, module)], module)
  end

  defp decode_documents([], carry, module) do
    carry
  end

  defp decode_document(document, module) when is_map(document) do
    # I hate this
    document
    |> Map.get("_source")
    |> Poison.encode!()
    |> Poison.decode!(as: struct(module, %{}))
  end
end
