defmodule Bungee.Response.Returner do
  @moduledoc """
  Prepare a tuple return for the request based on the response from Elasticsearch
  """

  alias Bungee.Response.Decoder

  @doc """
  For requests that create documents in Elasticsearch, return a tuple:

  `{:ok, :created, {identifier, document = any()}}`
  """
  def return({%Tesla.Env{}, :not_found}, _identifier, _document) do
    {:ok, :not_found}
  end

  def return({%Tesla.Env{body: %{"_id" => identifier}}, :created}, nil, document) do
    {:ok, :created, {identifier, document}}
  end

  def return({%Tesla.Env{}, :created}, identifier, document) do
    {:ok, :created, {identifier, document}}
  end

  def return({%Tesla.Env{}, :updated}, identifier, document) do
    {:ok, :updated, {identifier, document}}
  end

  def return({%Tesla.Env{}, :deleted}, identifier, nil) do
    {:ok, :deleted, identifier}
  end

  def return({%Tesla.Env{status: 200, body: body}, :ok}, identifier, nil, module) do
    {:ok, :found, {identifier, Decoder.decode!(body, module)}}
  end

  def return({%Tesla.Env{status: 200, body: %{"hits" => %{"total" => 0}}}, :ok}, nil, _module) do
    {:ok, 0, []}
  end

  def return(
        {%Tesla.Env{status: 200, body: %{"hits" => %{"hits" => documents, "total" => count}}},
         :ok},
        nil,
        module
      ) do
    {:ok, count, Decoder.decode!(documents, count, module)}
  end

  def return({%Tesla.Env{status: 404}, :not_found}, identifier, nil, module) do
    {:ok, :not_found}
  end

  def return({%Tesla.Env{status: status, body: body}, :error}, _, _)
      when status >= 400 and status <= 499 do
    {:error, :bad_request, body}
  end

  def return({%Tesla.Env{status: status, body: body}, :error}, _, _)
      when status >= 500 and status <= 599 do
    {:error, :server_error, body}
  end
end
