defmodule Bungee.Config do
  @type t :: %__MODULE__{uri: binary(), index: binary() | nil}
  defstruct uri: "http://localhost:9200", index: nil
end
