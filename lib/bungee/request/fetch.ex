defmodule Bungee.Request.Fetch do
  defstruct identifier: nil, cluster: nil, index_config: %{}, type: :get, request_options: []
end
