defmodule Bungee.Request.Builder do
  alias Bungee.Config
  alias Bungee.Request.Builder.AddDocument
  alias Bungee.Request.Builder.AddIdentifier
  alias Bungee.Request.Builder.AddIndex
  alias Bungee.Request.Builder.AddMethod
  alias Bungee.Request.Builder.AddQuery
  alias Bungee.Request.Builder.AddRequestOptions
  alias Bungee.Request.Builder.AddSearchParameters
  alias Bungee.Request.Builder.AddSort
  alias Bungee.Request.Builder.AddType

  @spec init(list(any()), Config.t()) :: nonempty_list(any())

  def init(request, config = %Config{}) do
    request
    |> add_uri(config.uri)
    |> add_json_content_type()
  end

  defp add_uri(request, uri) do
    request
    |> Keyword.put(
      :url,
      (fn uri ->
         String.trim_trailing(uri, "/")
       end).(uri)
    )
  end

  defp add_json_content_type(request) do
    Keyword.put(request, :headers, %{"Content-type" => "application/json"})
  end

  def encode_body(request) do
    body = Keyword.get(request, :body, nil)

    case body do
      nil -> request
      _ -> Keyword.put(request, :body, Poison.encode!(body))
    end
  end

  defdelegate add_document(request, document), to: AddDocument
  defdelegate add_identifier(request, identifier), to: AddIdentifier
  defdelegate add_index(request, index), to: AddIndex
  defdelegate add_method(request, method), to: AddMethod
  defdelegate add_query(request, query), to: AddQuery
  defdelegate add_request_options(request, request_options), to: AddRequestOptions
  defdelegate add_search_parameters(request, field, term), to: AddSearchParameters
  defdelegate add_sort(request, sort), to: AddSort
  defdelegate add_type(request, type), to: AddType
end
