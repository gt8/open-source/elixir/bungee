defmodule Bungee.Request.Builder.AddQuery do
  def add_query(request, query) do
    body =
      request
      |> Keyword.get(:body, %{})
      |> Map.put(:query, query)

    Keyword.put(request, :body, body)
  end
end
