defmodule Bungee.Request.Builder.AddSort do
  def add_sort(request, sorts) do
    sorts = add_sort_option([], Enum.reverse(sorts))

    body =
      request
      |> Keyword.get(:body, %{})
      |> Map.put(:sort, sorts)

    Keyword.put(request, :body, body)
  end

  defp add_sort_option(sorts, []) do
    sorts
  end

  defp add_sort_option(sorts, [{key, order} | tail]) when is_binary(key) do
    add_sort_option(sorts, [{String.to_atom(key), order}] ++ tail)
  end

  defp add_sort_option(sorts, [{key, order} | tail]) when is_binary(order) do
    add_sort_option(sorts, [{key, String.to_atom(order)}] ++ tail)
  end

  defp add_sort_option(sorts, [{key, order} | tail]) when is_atom(key) and is_atom(order) do
    [%{key => %{order: order}}]
    |> Kernel.++(sorts)
    |> add_sort_option(tail)
  end
end
