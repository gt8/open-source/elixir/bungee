defmodule Bungee.Request.Builder.AddIndex do
  def add_index(request, index) do
    request
    |> Keyword.update!(:url, fn url ->
      url <> "/" <> index
    end)
  end
end
