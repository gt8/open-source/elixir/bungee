defmodule Bungee.Request.Builder.AddIdentifier do
  def add_identifier(request, nil) do
    request
  end

  def add_identifier(request, identifier) do
    request
    |> Keyword.update!(:url, fn url ->
      url <> "/" <> identifier
    end)
  end
end
