defmodule Bungee.Request.Builder.AddRequestOptions do
  def add_request_options(request, []) do
    # Only allow force_refresh on endpoints that modify the index
    if Keyword.get(request, :method) not in [:head, :get] and
         Keyword.get(request, :url) =~ "?refresh=" and
         true == Application.get_env(:bungee, :force_refresh, false) do
      # This used to use `wait_for` instead of `true`, as I'd initially preferred
      # to wait for a refresh to happen organically. There are issues with this,
      # documented here: https://gitlab.com/gt8/open-source/elixir/bungee/issues/20
      add_request_option(request, {:refresh, true})
    else
      request
    end
  end

  def add_request_options(request, [head | tail]) do
    request
    |> add_request_option(head)
    |> add_request_options(tail)
  end

  def add_request_option(request, {:sort, sort}) do
    Bungee.Request.Builder.AddSort.add_sort(request, sort)
  end

  def add_request_option(request, {:refresh, true}) do
    request
    |> Keyword.update!(:url, fn url ->
      url <> "?refresh=true"
    end)
  end

  def add_request_option(request, {:refresh, false}) do
    request
    |> Keyword.update!(:url, fn url ->
      url <> "?refresh=false"
    end)
  end

  def add_request_option(request, {:refresh, :wait_for}) do
    request
    |> Keyword.update!(:url, fn url ->
      url <> "?refresh=wait_for"
    end)
  end

  def add_request_option(request, {:search, true}) do
    request
    |> Keyword.update!(:url, fn url ->
      url <> "/_search"
    end)
  end

  def add_request_option(request, {:size, size}) do
    Bungee.Request.Builder.AddSize.add_size(request, size)
  end
end
