defmodule Bungee.Request.Builder.AddDocument do
  @doc """
  Add a document to the body of our request. This function is only ever called
  when saving a new document; as such, it will fail if there's already something
  been added to the request's body.
  """
  def add_document(request, document) do
    Keyword.put_new(request, :body, document)
  end
end
