defmodule Bungee.Request.Builder.AddMethod do
  def add_method(request, method) do
    Keyword.put_new(request, :method, method)
  end
end
