defmodule Bungee.Request.Builder.AddSize do
  def add_size(request, size) do
    body =
      request
      |> Keyword.get(:body, %{})
      |> Map.put(:size, size)

    Keyword.put(request, :body, body)
  end
end
