defmodule Bungee.Request.Builder.AddSearchParameters do
  def add_search_parameters(request, field, term) do
    Keyword.put_new(request, :body, %{
      query: %{
        term: %{
          filter_key(field) => filter_value(term)
        }
      }
    })
  end

  defp filter_key(key) when is_atom(key) do
    Atom.to_string(key)
  end

  defp filter_key(key) when is_binary(key) do
    key
  end

  defp filter_value(value) when is_integer(value) do
    value
  end

  defp filter_value(value) when is_binary(value) do
    value
  end
end
