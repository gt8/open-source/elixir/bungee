defmodule Bungee.Request.Builder.AddType do
  def add_type(request, type) do
    request
    |> Keyword.update!(:url, fn url ->
      url <> "/" <> type
    end)
  end
end
