defmodule Bungee.Mixfile do
  use Mix.Project

  def project do
    [
      app: :bungee,
      version: "1.0.0-alpha4",
      elixir: "~> 1.3",
      build_embedded: Mix.env() == :prod,
      start_permanent: Mix.env() == :prod,
      deps: deps(),
      elixirc_paths: elixirc_paths(Mix.env()),
      aliases: aliases(),
      description: description(),
      package: package(),
      test_coverage: [tool: ExCoveralls],
      docs: [main: "readme", extras: ["README.md"]]
    ]
  end

  def application do
    [
      applications: [
        :hackney,
        :logger
      ]
    ]
  end

  def deps do
    [
      {:hackney, "~> 1.10"},
      {:poison, ">= 3.1.0"},
      {:tesla, "~> 0.9.0"},
      {:uuid, "~> 1.1"},
      {:dialyxir, "~> 0.5", only: [:dev], runtime: false},
      {:ex_doc, ">= 0.0.0", only: :dev},
      {:excoveralls, "~> 0.7", only: :test},
      {:faker, "~> 0.9", only: :test}
    ]
  end

  def aliases do
    [test: ["coveralls"]]
  end

  defp description do
    """
    Elasticsearch Client with Repository implementation
    """
  end

  defp package do
    [
      name: :bungee,
      files: ["lib", "mix.exs", "README.md", "LICENSE"],
      maintainers: ["GT8Online"],
      licenses: ["MIT"],
      links: %{"Source Code" => "https://gitlab.com/gt8/open-source/elixir/bungee"}
    ]
  end

  defp elixirc_paths(:dev), do: ["lib"]
  defp elixirc_paths(:test), do: ["test"] ++ elixirc_paths(:dev)
  defp elixirc_paths(_), do: ["lib"]
end
