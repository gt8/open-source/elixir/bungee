defmodule Bungee.Support.KeyedType do
  defstruct identifier: nil, forename: nil, surname: nil, emails: []

  def key(type = %__MODULE__{}) do
    type.identifier
  end
end

defmodule Bungee.Support.KeyedRepository do
  @moduledoc false
  use Bungee.Repository, module: Bungee.Support.KeyedType, type: "keyed"
end
