defmodule Bungee.Support.UnkeyedType do
  defstruct forename: nil, surname: nil, emails: []
end

defmodule Bungee.Support.UnkeyedRepository do
  @moduledoc false
  use Bungee.Repository, module: Bungee.Support.UnkeyedType, type: "unkeyed"
end
