defmodule Bungee.DeleteTest do
  # Tests cannot be async, as we're modifying the Application configuration
  use ExUnit.Case, async: false
  alias Bungee.Support.UnkeyedRepository, as: BungeeRepository

  setup do
    %{index: index, uri: uri} = Application.get_env(:bungee, :config)

    identifier = UUID.uuid4()

    object = %Bungee.Support.UnkeyedType{
      forename: Faker.Name.first_name(),
      surname: Faker.Name.last_name(),
      emails: [Faker.Internet.email()]
    }

    %Tesla.Env{status: 201} =
      Tesla.post(
        "#{uri}/#{index}/unkeyed/#{identifier}?refresh",
        Poison.encode!(object),
        headers: %{"Content-Type" => "application/json"}
      )

    {:ok, %{object: object, identifier: identifier}}
  end

  test ~s/it can delete objects by identifier/, context do
    assert {:ok, :deleted, context.identifier} == BungeeRepository.delete(context.identifier)
    assert {:ok, :not_found} == BungeeRepository.fetch(context.identifier)
  end
end
