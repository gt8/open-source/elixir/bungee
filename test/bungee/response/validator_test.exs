defmodule Bungee.Response.ValidatorTest do
  use ExUnit.Case, async: true
  alias Bungee.Response.Validator
  alias Bungee.Support.HttpRequestMock

  setup do
    Tesla.Mock.mock(fn
      %{method: :get, url: "https://example.com"} ->
        %Tesla.Env{status: 200}

      %{method: :post, url: "https://example.com"} ->
        %Tesla.Env{status: 201}

      %{method: :delete, url: "https://example.com"} ->
        %Tesla.Env{status: 202}

      %{method: :get, url: "https://example.com/unknown"} ->
        %Tesla.Env{status: 500}
    end)

    :ok
  end

  test ~s/it returns responds correctly when the request is OK/ do
    assert {_, :ok} =
             Validator.response_ok?(
               HttpRequestMock.request(method: :get, url: "https://example.com")
             )
  end

  test ~s/it returns responds correctly when a new resource is created/ do
    assert {_response, :created} =
             Validator.response_ok?(
               HttpRequestMock.request(method: :post, url: "https://example.com")
             )
  end

  test ~s/it returns responds correctly when a resource is deleted/ do
    assert {_response, :deleted} =
             Validator.response_ok?(
               HttpRequestMock.request(method: :delete, url: "https://example.com")
             )
  end

  test ~s/it returns responds correctly when the request status code isn't known/ do
    assert {_response, :error} =
             Validator.response_ok?(
               HttpRequestMock.request(method: :get, url: "https://example.com/unknown")
             )
  end
end
