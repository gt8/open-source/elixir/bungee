defmodule Bungee.Response.ReturnerTest do
  use ExUnit.Case, async: true
  alias Bungee.Response.Returner

  test ~s/it can handle single a creation without an identifier/ do
    document = %{forename: "David"}

    response =
      %Tesla.Env{body: body} =
      Tesla.post(
        "http://elasticsearch:9200/index_write/my_type",
        Poison.encode!(document),
        headers: %{"Content-Type" => "application/json"}
      )

    object_response = Poison.decode!(body)

    assert true = Map.has_key?(object_response, "_id")
    assert object_response["created"] == true

    assert {:ok, :created, {_, ^document}} = Returner.return({response, :created}, nil, document)
  end

  test ~s/it can handle single a creation with an identifier/ do
    uuid = UUID.uuid4()
    document = %{forename: "David"}

    response =
      %Tesla.Env{body: body} =
      Tesla.post(
        "http://elasticsearch:9200/index_write/my_type/#{uuid}",
        Poison.encode!(document),
        headers: %{"Content-Type" => "application/json"}
      )

    object_response = Poison.decode!(body)

    assert object_response["_id"] == uuid
    assert object_response["created"] == true

    assert {:ok, :created, {^uuid, ^document}} =
             Returner.return({response, :created}, uuid, document)
  end

  test ~s/it can handle a single update/ do
    uuid = UUID.uuid4()
    document = %{forename: "David"}

    Tesla.post(
      "http://elasticsearch:9200/index_write/my_type/#{uuid}",
      Poison.encode!(document),
      headers: %{"Content-Type" => "application/json"}
    )

    response =
      %Tesla.Env{body: body} =
      Tesla.post(
        "http://elasticsearch:9200/index_write/my_type/#{uuid}",
        Poison.encode!(document),
        headers: %{"Content-Type" => "application/json"}
      )

    object_response = Poison.decode!(body)

    assert object_response["_id"] == uuid
    assert object_response["result"] == "updated"

    assert {:ok, :updated, {^uuid, ^document}} =
             Returner.return({%{response | body: object_response}, :updated}, uuid, document)
  end

  test ~s/it can handle a single delete/ do
    uuid = UUID.uuid4()
    document = %{forename: "David"}

    Tesla.post(
      "http://elasticsearch:9200/index_write/my_type/#{uuid}",
      Poison.encode!(document),
      headers: %{"Content-Type" => "application/json"}
    )

    response =
      %Tesla.Env{body: body} =
      Tesla.delete(
        "http://elasticsearch:9200/index_write/my_type/#{uuid}",
        headers: %{"Content-Type" => "application/json"}
      )

    object_response = Poison.decode!(body)

    assert object_response["_id"] == uuid
    assert object_response["result"] == "deleted"

    assert {:ok, :deleted, ^uuid} =
             Returner.return({%{response | body: object_response}, :deleted}, uuid, nil)
  end

  test ~s/it can handle a single fetch/ do
    uuid = UUID.uuid4()
    document = %{forename: "David"}

    Tesla.post(
      "http://elasticsearch:9200/index_write/my_type/#{uuid}",
      Poison.encode!(document),
      headers: %{"Content-Type" => "application/json"}
    )

    response =
      %Tesla.Env{body: body} =
      Tesla.get(
        "http://elasticsearch:9200/index_write/my_type/#{uuid}",
        headers: %{"Content-Type" => "application/json"}
      )

    object_response = Poison.decode!(body)

    assert object_response["_id"] == uuid

    assert {:ok, :found, {^uuid, _documents}} =
             Returner.return(
               {%{response | body: object_response}, :ok},
               uuid,
               nil,
               Bungee.Support.KeyedType
             )
  end
end
