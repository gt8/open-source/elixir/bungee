defmodule Bungee.Response.DecoderTest do
  use ExUnit.Case, async: true
  alias Bungee.Response.Decoder

  test ~s/it returns an empty list when no documents are found/ do
    documents = []
    assert {:ok, 0, []} == Decoder.decode(documents, 0, %{})
  end

  test ~s/a single document is decoded/ do
    document = %{
      "_id" => "test",
      "_source" => %{
        forename: "Scott"
      }
    }

    {:ok, %Bungee.Support.KeyedType{} = decoded} =
      Decoder.decode(document, %Bungee.Support.KeyedType{})

    assert decoded.forename == "Scott"
  end

  test ~s/multiple documents are decoded and returned/ do
    first_forename = Faker.Name.first_name()
    second_forename = Faker.Name.first_name()

    documents = [
      %{
        "_id" => UUID.uuid4(),
        "_source" => %{
          forename: first_forename
        }
      },
      %{
        "_id" => UUID.uuid4(),
        "_source" => %{
          forename: second_forename
        }
      }
    ]

    {:ok, count, decoded} = Decoder.decode(documents, 2, %Bungee.Support.KeyedType{})

    assert count == 2

    assert List.first(decoded).forename == first_forename
    assert List.last(decoded).forename == second_forename
  end
end
