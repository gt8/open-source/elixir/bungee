defmodule Bungee.FetchTest do
  # Tests cannot be async, as we're modifying the Application configuration
  use ExUnit.Case, async: false
  alias Bungee.Support.UnkeyedRepository, as: BungeeRepository

  setup do
    %{index: index, uri: uri} = Application.get_env(:bungee, :config)

    object = %Bungee.Support.UnkeyedType{
      forename: Faker.Name.first_name(),
      surname: Faker.Name.last_name(),
      emails: [Faker.Internet.email()]
    }

    identifier = UUID.uuid4()

    %Tesla.Env{status: 201} =
      Tesla.post(
        "#{uri}/#{index}/unkeyed/#{identifier}?refresh",
        Poison.encode!(object),
        headers: %{"Content-Type" => "application/json"}
      )

    {:ok, %{object: object, identifier: identifier}}
  end

  test ~s/it can handle fetch requests for objects that do not exist/ do
    assert {:ok, :not_found} = BungeeRepository.fetch("unknown_identifier")
  end

  test ~s/it can fetch objects by identifier/, context do
    assert {:ok, :found, {context.identifier, context.object}} ==
             BungeeRepository.fetch(context.identifier)
  end
end
