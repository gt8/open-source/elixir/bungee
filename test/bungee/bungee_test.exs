defmodule BungeeTest do
  use ExUnit.Case, async: true

  setup_all do
    %{index: index, uri: uri} = Application.get_env(:bungee, :config)

    {:ok, %{uri: uri, index: index}}
  end

  test ~s/it can save a new record/, %{uri: uri, index: index} do
    forename = Faker.Name.first_name()

    {:ok, :created, {identifier, %{forename: ^forename}}} =
      GenServer.call(
        :bungee,
        {:save, index, "my_type", [refresh: :wait_for], nil, %{forename: forename}}
      )

    response = %Tesla.Env{body: body} = Tesla.get("#{uri}/#{index}/my_type/#{identifier}")

    document = Poison.decode!(body)

    assert %{"forename" => ^forename} = document["_source"]
  end

  test ~s/it can save an old record/, %{uri: uri, index: index} do
    forename = Faker.Name.first_name()

    {:ok, :created, {identifier, %{forename: ^forename}}} =
      GenServer.call(
        :bungee,
        {:save, index, "my_type", [refresh: :wait_for], nil, %{forename: forename}}
      )

    updated_forename = Faker.Name.first_name()

    {:ok, :updated, {^identifier, %{forename: ^updated_forename}}} =
      GenServer.call(
        :bungee,
        {:save, index, "my_type", [refresh: :wait_for], identifier, %{forename: updated_forename}}
      )

    %Tesla.Env{body: body} = Tesla.get("#{uri}/#{index}/my_type/#{identifier}")

    document = Poison.decode!(body)

    assert document["_source"] == %{"forename" => updated_forename}
  end

  test ~s/it can delete a record/, %{uri: uri, index: index} do
    identifier = UUID.uuid4()

    %Tesla.Env{status: 201} =
      Tesla.post(
        "#{uri}/#{index}/my_type/#{identifier}?refresh=wait_for",
        Poison.encode!(%{forename: Faker.Name.first_name()}),
        headers: %{"Content-Type" => "application/json"}
      )

    {:ok, :deleted, ^identifier} =
      GenServer.call(
        :bungee,
        {:delete, index, "my_type", [refresh: :wait_for], identifier, nil}
      )
  end

  test ~s/it can fetch a record/, %{uri: uri, index: index} do
    identifier = UUID.uuid4()

    %Tesla.Env{status: 201} =
      Tesla.post(
        "#{uri}/#{index}/my_type/#{identifier}?refresh=wait_for",
        Poison.encode!(%{forename: Faker.Name.first_name()}),
        headers: %{"Content-Type" => "application/json"}
      )

    {:ok, :found, {^identifier, _document}} =
      GenServer.call(
        :bungee,
        {:fetch, index, "my_type", [], identifier, Bungee.Support.KeyedType, nil}
      )
  end

  test ~s/it can fetch a record by a property/, %{uri: uri, index: index} do
    identifier = UUID.uuid4()
    surname = Faker.Name.last_name()

    %Tesla.Env{status: 201} =
      Tesla.post(
        "#{uri}/#{index}/my_type/#{identifier}?refresh=wait_for",
        Poison.encode!(%{forename: Faker.Name.first_name(), surname: surname}),
        headers: %{"Content-Type" => "application/json"}
      )

    {:ok, 1, _documents} =
      GenServer.call(
        :bungee,
        {:fetch_by, index, "my_type", [], :surname, surname, Bungee.Support.KeyedType}
      )
  end
end
