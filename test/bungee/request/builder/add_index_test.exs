defmodule Bungee.Request.Builder.AddIndexTest do
  use ExUnit.Case, async: true
  alias Bungee.Request.Builder.AddIndex, as: UUT

  test ~s/it can add an index to an existing request structure/ do
    assert [url: "http://localhost:9200/my_index"] =
             UUT.add_index([url: "http://localhost:9200"], "my_index")
  end
end
