defmodule Bungee.Request.Builder.AddRequestOptionTest do
  use ExUnit.Case, async: true
  alias Bungee.Request.Builder.AddRequestOptions, as: UUT

  test ~s/it can add refresh request option to a request/ do
    assert [url: "http://localhost:9200/my_index?refresh=true"] =
             UUT.add_request_options(
               [url: "http://localhost:9200/my_index"],
               refresh: true
             )

    assert [url: "http://localhost:9200/my_index?refresh=false"] =
             UUT.add_request_options(
               [url: "http://localhost:9200/my_index"],
               refresh: false
             )

    assert [url: "http://localhost:9200/my_index?refresh=wait_for"] =
             UUT.add_request_options(
               [url: "http://localhost:9200/my_index"],
               refresh: :wait_for
             )
  end

  test ~s/it can add a search request option to a request/ do
    assert [url: "http://localhost:9200/my_index/_search"] =
             UUT.add_request_options(
               [url: "http://localhost:9200/my_index"],
               search: true
             )
  end

  test ~s/it can add a sort preferences to a request/ do
    assert [
             body: %{
               sort: [
                 %{name: %{order: :asc}}
               ]
             },
             url: "http://localhost:9200/my_index"
           ] =
             UUT.add_request_options(
               [url: "http://localhost:9200/my_index"],
               sort: [{:name, :asc}]
             )
  end

  test ~s/it can add size options to a request/ do
    assert [
             body: %{
               size: 2
             },
             url: "http://localhost:9200/my_index"
           ] =
             UUT.add_request_options(
               [url: "http://localhost:9200/my_index"],
               size: 2
             )
  end
end
