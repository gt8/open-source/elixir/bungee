defmodule Bungee.Request.Builder.AddDocumentTest do
  use ExUnit.Case, async: true
  alias Bungee.Request.Builder.AddDocument, as: UUT

  test ~s/it can add a document to a request/ do
    document = %{
      forename: "David"
    }

    assert [body: ^document, url: "http://localhost:9200/my_index"] =
             UUT.add_document(
               [url: "http://localhost:9200/my_index"],
               document
             )
  end
end
