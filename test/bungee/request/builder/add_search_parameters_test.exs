defmodule Bungee.Request.Builder.AddSearchParametersTest do
  use ExUnit.Case, async: true
  alias Bungee.Request.Builder.AddSearchParameters, as: UUT

  test ~s/it can add a query to a request/ do
    assert [
             body: %{
               query: %{
                 term: %{"forename" => "David"}
               }
             },
             url: "http://localhost:9200/my_index"
           ] =
             UUT.add_search_parameters(
               [url: "http://localhost:9200/my_index"],
               "forename",
               "David"
             )
  end
end
