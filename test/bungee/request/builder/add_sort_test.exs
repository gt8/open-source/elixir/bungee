defmodule Bungee.Request.Builder.AddSortTest do
  use ExUnit.Case, async: true
  alias Bungee.Request.Builder.AddSort, as: UUT

  test ~s/it can add a sort to to a request with no query/ do
    assert [
             body: %{
               sort: [
                 %{name: %{order: :asc}},
                 %{age: %{order: :desc}}
               ]
             },
             url: "http://localhost:9200/my_index"
           ] =
             UUT.add_sort([url: "http://localhost:9200/my_index"], [{:name, :asc}, {:age, "desc"}])
  end

  test ~s/it can add a sort to to a request with a query/ do
    assert [
             body: %{
               query: %{match: %{name: "David"}},
               sort: [%{name: %{order: :asc}}, %{age: %{order: :desc}}]
             },
             url: "http://localhost:9200/my_index"
           ] =
             UUT.add_sort(
               [
                 url: "http://localhost:9200/my_index",
                 body: %{query: %{match: %{name: "David"}}}
               ],
               [{"name", :asc}, {:age, "desc"}]
             )
  end
end
