defmodule Bungee.Request.Builder.AddSizeTest do
  use ExUnit.Case, async: true
  alias Bungee.Request.Builder.AddSize, as: UUT

  test ~s/it can add a size to to a request with no query/ do
    assert [
             body: %{
               size: 2
             },
             url: "http://localhost:9200/my_index"
           ] = UUT.add_size([url: "http://localhost:9200/my_index"], 2)
  end

  test ~s/it can add a size to a request with a query/ do
    assert [
             body: %{
               query: %{match: %{name: "David"}},
               size: 2
             },
             url: "http://localhost:9200/my_index"
           ] =
             UUT.add_size(
               [
                 url: "http://localhost:9200/my_index",
                 body: %{query: %{match: %{name: "David"}}}
               ],
               2
             )
  end
end
