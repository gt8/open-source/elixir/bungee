defmodule Bungee.Request.Builder.AddMethodTest do
  use ExUnit.Case, async: true
  alias Bungee.Request.Builder.AddMethod, as: UUT

  test ~s/it can add the method to a request/ do
    assert [method: :put] =
             UUT.add_method(
               [],
               :put
             )

    assert [method: :delete] =
             UUT.add_method(
               [],
               :delete
             )
  end
end
