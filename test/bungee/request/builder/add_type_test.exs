defmodule Bungee.Request.Builder.AddTypeTest do
  use ExUnit.Case, async: true
  alias Bungee.Request.Builder.AddType, as: UUT

  test ~s/it can add a type to an existing request structure/ do
    assert [url: "http://localhost:9200/my_index/my_type"] =
             UUT.add_type([url: "http://localhost:9200/my_index"], "my_type")
  end
end
