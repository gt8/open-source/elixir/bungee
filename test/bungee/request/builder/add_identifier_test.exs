defmodule Bungee.Request.Builder.AddIdentifierTest do
  use ExUnit.Case, async: true
  alias Bungee.Request.Builder.AddIdentifier, as: UUT

  test ~s/it can add an identifier to an existing request structure/ do
    assert [url: "http://localhost:9200/my_index/my_type/id1"] =
             UUT.add_identifier([url: "http://localhost:9200/my_index/my_type"], "id1")

    assert [url: "http://localhost:9200/my_index/my_type"] =
             UUT.add_identifier([url: "http://localhost:9200/my_index/my_type"], nil)
  end
end
