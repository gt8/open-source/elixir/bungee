defmodule Bungee.Request.BuilderTest do
  use ExUnit.Case, async: true
  alias Bungee.Config
  alias Bungee.Request.Builder

  setup_all do
    {:ok,
     %{
       config: %Config{
         uri: "http://localhost:9200",
         index: "bungee_test_index"
       }
     }}
  end

  test ~s/it can bootstrap a request/, context do
    assert [headers: %{"Content-type" => "application/json"}, url: "http://localhost:9200"] =
             Builder.init(
               [],
               context.config
             )

    assert [headers: %{"Content-type" => "application/json"}, url: "http://localhost:9200"] =
             Builder.init(
               [],
               context.config
             )
  end
end
