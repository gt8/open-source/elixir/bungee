defmodule Bungee.CustomQueriesTest do
  use ExUnit.Case, async: false
  alias Bungee.Support.UnkeyedRepository, as: BungeeRepository

  setup do
    %{index: index, uri: uri} = Application.get_env(:bungee, :config)

    surname = Faker.Name.last_name()

    # Create 10 Random UnkeyedType's and store in Elasticsearch, each sharing
    # the same `surname` property.
    for _ <- 1..10 do
      %Tesla.Env{status: 201} =
        Tesla.post(
          "#{uri}/#{index}/unkeyed/#{UUID.uuid4()}?refresh",
          Poison.encode!(%Bungee.Support.UnkeyedType{
            forename: Faker.Name.first_name(),
            surname: surname,
            emails: [Faker.Internet.email()]
          }),
          headers: %{"Content-Type" => "application/json"}
        )
    end

    {:ok, %{surname: surname}}
  end

  test ~s/it can handle fetch requests for objects that do not exist/, %{surname: surname} do
    assert {:ok, 10, _} =
             BungeeRepository.query(%{
               match: %{
                 surname: surname
               }
             })
  end
end
