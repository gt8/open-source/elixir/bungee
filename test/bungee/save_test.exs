defmodule Bungee.SaveTest do
  # Tests cannot be async, as we're modifying the Application configuration
  use ExUnit.Case, async: false
  alias Bungee.Support.KeyedRepository, as: BungeeRepository

  test ~s/it can save an object that do not exist yet/ do
    identifier = UUID.uuid4()

    record = %Bungee.Support.KeyedType{
      identifier: identifier,
      forename: Faker.Name.first_name(),
      surname: Faker.Name.last_name(),
      emails: [Faker.Internet.email()]
    }

    assert {:ok, :created, {^identifier, ^record}} = BungeeRepository.save(record)
  end

  test ~s/It can save an object that already exists/ do
    identifier = UUID.uuid4()

    record = %Bungee.Support.KeyedType{
      identifier: identifier,
      forename: Faker.Name.first_name(),
      surname: Faker.Name.last_name(),
      emails: [Faker.Internet.email()]
    }

    BungeeRepository.save(record, refresh: :wait_for)

    email = Faker.Internet.email()

    assert {:ok, :updated, {^identifier, _new_record}} =
             BungeeRepository.save(
               %Bungee.Support.KeyedType{record | emails: [email]},
               refresh: :wait_for
             )
  end
end
