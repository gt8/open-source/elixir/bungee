defmodule Bungee.ConfigTest do
  use ExUnit.Case, async: true
  alias Bungee.Config

  test ~s/it has sensible default values/ do
    assert %Config{uri: "http://localhost:9200", index: nil} = %Config{}
  end
end
