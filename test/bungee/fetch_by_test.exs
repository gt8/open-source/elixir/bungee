defmodule Bungee.FetchByTest do
  # Tests cannot be async, as we're modifying the Application configuration
  use ExUnit.Case, async: false
  alias Bungee.Support.UnkeyedRepository, as: BungeeRepository

  setup do
    %{index: index, uri: uri} = Application.get_env(:bungee, :config)

    identifier = UUID.uuid4()
    surname = Faker.Name.last_name()
    forenames = []

    forenames =
      1..10
      |> Enum.map(fn counter ->
        forename = Faker.Name.first_name()

        object = %Bungee.Support.UnkeyedType{
          forename: forename,
          surname: surname,
          emails: [Faker.Internet.email()]
        }

        %Tesla.Env{status: 201} =
          Tesla.post(
            "#{uri}/#{index}/unkeyed/#{identifier}-#{counter}?refresh=true",
            Poison.encode!(object),
            headers: %{"Content-Type" => "application/json"}
          )

        forename
      end)

    {:ok, %{forenames: forenames, surname: surname, identifier: identifier}}
  end

  test ~s/it can handle fetch requests for objects that do not exist/ do
    # I originally made this `first_name` ... then figured; what if they get the same one? :joy:
    assert {:ok, 0, []} = BungeeRepository.fetch_by(:forename, Faker.Name.last_name())
  end

  test ~s/it can fetch objects by key => value matches/, context do
    assert {:ok, 10, _} = BungeeRepository.fetch_by(:surname, context.surname)
  end

  test ~s/it allows sorting parameters within the fetch/, context do
    sorted_forenames = Enum.sort(context.forenames)

    assert {:ok, 10, objects} =
             BungeeRepository.fetch_by(:surname, context.surname, sort: [forename: :asc])

    assert sorted_forenames ==
             Enum.map(objects, fn object ->
               object.forename
             end)
  end
end
