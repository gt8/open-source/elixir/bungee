Application.put_env(:bungee, :config, %{
  uri: System.get_env("ELASTICSEARCH_URI"),
  index: System.get_env("ELASTICSEARCH_INDEX")
})

Bungee.start_link(:bungee)
Faker.start()
ExUnit.start()
