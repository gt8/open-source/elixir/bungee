ELASTICSEARCH_PORT_9200_TCP_ADDR?=elasticsearch
ELASTICSEARCH_PORT_9200_TCP_PORT?=9200
TESTS=

.PHONY: test deps

format-check:
	@mix format --check-formatted

format:
	@mix format --check-equivalent

init: /root/.mix

/root/.mix:
	@mix local.rebar --force
	@mix local.hex --force

deps: init
	@mix deps.get

compile:
	@mix compile

lint:
	@mix credo --strict

test: es
	@mix test $(TESTS)

analyse:
	@mix dialyzer

clean:
	@mix clean --deps

# Work within Docker
es:
	-curl -s -X DELETE ${ELASTICSEARCH_URI}/${ELASTICSEARCH_INDEX}
	curl -s -X PUT -H "Content-Type: application/json" -d @opt/elasticsearch/mapping.json ${ELASTICSEARCH_URI}/${ELASTICSEARCH_INDEX}

dshell:
	@docker-compose run --rm --entrypoint=bash elixir

dclean:
	@docker-compose run --rm elixir clean --deps
	@docker-compose down -v
